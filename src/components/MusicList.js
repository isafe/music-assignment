import {useDispatch, useSelector} from "react-redux";
import {fetchAllSongs, fetchFavSong, selectSong} from "../actions/songAction.js";

import {motion} from "framer-motion"

import {Song} from "./Song";
import './MusicList.scss'
import './MusicList.css'
import {useEffect} from "react";



export const MusicList = () => {

    const songList = useSelector( state => state?.songReducer?.songList)

    const dispatch = useDispatch()

    const songFavList = useSelector(state => state?.songReducer?.favouriteSongList)



    useEffect(()=>{
        dispatch(fetchAllSongs())
        dispatch(fetchFavSong())

    },[])

    songList.forEach(v=>{
        if(songFavList.includes(v.id)){
            v.fav = true;
        }else{
            v.fav =false;
        }
    })
    console.log(songList)

    const variants = {
        container: {
            animate: {
                transition: {
                    staggerChildren: 0.05
                }
            }
        },
        card: {
            initial: {
                opacity: 0,
                y:-50
            },

            animate: {
                opacity: 1,
                y: 0
            }
        }
    };


    return <div className="musicListContainer">


        <motion.ul
            initial="initial"
            animate="animate"
            variants={variants.container}

        >
            {
                songList.map( (value,index)=>
                    <motion.li
                        variants={variants.card}
                        key={index}
                        className="songList"

                        onClick={
                            ()=>{
                                dispatch(selectSong(value.id))
                            }
                        }

                    >
                        <Song value={value}/>

                    </motion.li>
                )
            }
        </motion.ul>





    </div>
}
