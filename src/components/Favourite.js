import {useDispatch, useSelector} from "react-redux";
import {AnimatePresence, motion} from "framer-motion"
import {useEffect, useState} from "react";
import {addFavSong, deleteFavSong, fetchAllSongs, fetchFavSong} from "../actions/songAction";
import './Favourite.scss'






export const Favourite = ()=>{

    let songList = useSelector(state => state?.songReducer?.songList)
    const dispatch = useDispatch()



    const variants = {
        container: {
            animate: {
                transition: {
                    staggerChildren: 0.1,

                }
            }
        },
        card: {
            initial: {
                opacity: 0,
                x: -100
            },

            animate: {
                opacity: 1,
                x: 0
            },
            transition: {

            }
        }
    };





    return <div className='favContainer'>
        <h1 style={{color:'white'}}>Favourite</h1>
        <motion.ul
            initial="initial"
            animate="animate"
            variants={variants.container}
        >

            {songList.filter(x=>x.fav===true).map( (value, i)=>

                <motion.div
                    className='rowSong'
                    variants={variants.card}
                    key={i}
                >



                    {<div className='row'>

                        <img src={value.cover}/>
                        <div style={{width:'40%'}}>{value.title}</div>
                        <div style={{width:'40%'}}>{value.artist}</div>
                        <div style={{width:'10%'}}>${value.price}</div>
                        <button
                            className='deleteBtn'
                            onClick={e=>{
                                dispatch(deleteFavSong({value}.value.id))
                                e.target.parentNode.parentNode.style.display = 'none'

                            }}
                        >Delete</button>

                    </div>

                    }




                </motion.div>
            ) }
        </motion.ul>


    </div>
}