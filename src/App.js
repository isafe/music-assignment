import {MusicList} from "./components/MusicList";
import './App.scss'
import {Route, Routes} from "react-router-dom";
import {HomeNav} from "./components/HomeNav";
import {Player} from "./components/Player";
import {Favourite} from "./components/Favourite";



function App() {

  return (
    <div className="App">
      <HomeNav/>
      <Routes>
          <Route path='/' element={<MusicList/>}/>
          <Route path='/player' element={<Player/>}/>
          <Route path='/favourite' element ={<Favourite/>}/>
      </Routes>
    </div>
  );
}

export default App;
