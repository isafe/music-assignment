import {Link} from "react-router-dom";
import './HomeNav.scss'
import {BsMusicNoteBeamed, BsMusicPlayerFill} from 'react-icons/bs';
import {AiFillLike} from 'react-icons/ai';

export const HomeNav = () => {

    return (
        <nav className='homeNav'>
            <div>
                <Link to='/' className='link' >
                    <BsMusicNoteBeamed/>
                    <h6>ALL Song</h6>
                    </Link>
                <Link to='/player' className='link'>
                    <BsMusicPlayerFill/>
                    <h6>player</h6>
                </Link>
                <Link to='/favourite' className='link'>
                    <AiFillLike/>
                    <h6>favourite</h6>
                </Link>
            </div>

        </nav>
    )

}