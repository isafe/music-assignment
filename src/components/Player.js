import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import './Player.scss'
import {ImNext2, ImPause2, ImPlay3, ImPrevious2,ImHeart} from "react-icons/im";
import {addFavSong, deleteFavSong, fetchFavSong, selectSong} from "../actions/songAction";

import {motion} from "framer-motion";
import {useParams} from "react-router-dom";
import axios from "axios";
import {songs_URL} from "../helper";

export const Player = () => {


    const dispatch = useDispatch()
    let songId = useSelector(state => state?.songReducer?.songId)
    const songList = useSelector( state => state?.songReducer?.songList)
    const songFavList = useSelector( state => state?.songReducer?.favouriteSongList)
    const [start,setStart] = useState(true);
    const [fav,setFav] = useState(null);
    let {playId}= useParams();

    const fetchById = ()=>{
        axios.get(songs_URL+{playId})
            .then( data => {
                console.log(data)
            })
    }

    useEffect(fetchById,[])


    useEffect(()=>{

        dispatch(fetchFavSong())
        if(songFavList.includes(songId)){
            setFav(true)
            document.getElementById('playerFav').className = 'playerFav addFavBtn'
        }else{
            setFav(false)
            document.getElementById('playerFav').className = 'playerFav notFavBtn'
        }
        document.getElementById('player').play()
        setStart(true)
        document.getElementById('backImg').style.animationPlayState = 'running'
        document.getElementById('pause').style.display = 'block'
        document.getElementById('play').style.display = 'none'
    },[songId])

    const playBtnHandler = () => {
        if(start) {
            document.getElementById('player').pause()
            setStart(false)
            document.getElementById('backImg').style.animationPlayState = 'paused'
            document.getElementById('pause').style.display = 'none'
            document.getElementById('play').style.display = 'block'
        }
        else{
            document.getElementById('player').play()
            setStart(true)
            document.getElementById('backImg').style.animationPlayState = 'running'
            document.getElementById('pause').style.display = 'block'
            document.getElementById('play').style.display = 'none'
        }
    }

    const favBtnHandler = () => {
        if(fav){
            setFav(false)
            dispatch(deleteFavSong(songId))
            document.getElementById('playerFav').className = 'playerFav notFavBtn'
            songList[songId-1].fav = false
        }else{
            setFav(true)
            dispatch(addFavSong(songId))
            document.getElementById('playerFav').className = 'playerFav addFavBtn'
            songList[songId-1].fav = true
        }

    }

    return(
        <div className='playerContainerWhole'>
            <img  src='https://images.unsplash.com/photo-1501612780327-45045538702b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80' />
            <motion.div  initial={{ opacity: 0 }} animate={{ opacity: 1}} drag className='playerContainer'>
                <button onClick={favBtnHandler} className='playerFav' id='playerFav'>
                    <ImHeart/>
                </button>
                <h2>Now Playing</h2>
                <audio id='player'
                       src={songList[songId-1]?.stream}
                       autoPlay={true}
                >

                </audio>
                <img id='backImg' src={songList[songId-1]?.cover} />
                <h4>Title: {songList[songId-1]?.title}</h4>
                <h5>Artist: {songList[songId-1]?.artist}</h5>
                <div className="bar">

                </div>

                <div className='btnControl'>
                    <button className='control'
                            onClick={()=>{
                                songId !== 1 && dispatch(selectSong(songId-1))
                                songId === 1 && dispatch(selectSong(songList.length))
                            }}
                    ><ImPrevious2/></button>
                    <button id= 'pause'
                            onClick={playBtnHandler}

                            className='control'><ImPause2/></button>
                    <button id='play' style={{display:'none'}}
                            onClick={playBtnHandler}

                            className='control'><ImPlay3/></button>
                    <button
                        onClick={()=>{
                            songId !== songList.length && dispatch(selectSong(songId+1))
                            songId === songList.length && dispatch(selectSong(1))
                        }}
                        className='control'><ImNext2/>
                    </button>


                </div>




            </motion.div>
            <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1}} drag className='list'>
              <ul>
                  {
                      songList.map( value =>
                          <li key={value.id}>
                              <img src={value.cover} alt=''/>
                          </li>

                      )
                  }
                  {
                      songList.map( value =>
                      <li key={value.id+songList.length}>
                      <img src={value.cover} alt=''/>
                      </li>

                      )
                  }
              </ul>
            </motion.div>
        </div>





    )

}


