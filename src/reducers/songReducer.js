import {
    ADD_FAVOURITE_SONG,
    DELETE_FAVOURITE_SONG,
    Delete_FAVOURITE_SONG,
    FETCH_ALL_SONGS, FETCH_FAVOURITE_SONG,
    SELECT_SONG
} from "../helper";

const initState = {
    //其中一个state是songList
    songList:[],

    //其中的里另外一个state是songId
    songId:1,

    favouriteSongList:[]
}

export const songReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_ALL_SONGS:
            //如果action为空，容易报错，safety navigation和swift那个一样
            console.log('song reducer is printing ====>',action?.payload)
            //在这里，因为immutable是redux的核心理念，所以这里是说，我们copy了一个新的state对象，然后override他的action属性
            return {...state,songList: action?.payload}
        case SELECT_SONG:
            return{...state,songId: action?.payload}

        case ADD_FAVOURITE_SONG:

            !state.favouriteSongList.includes(action?.payload) && state.favouriteSongList.push(action?.payload)

            return{...state,favouriteSongList: state.favouriteSongList}
        case DELETE_FAVOURITE_SONG:

            let index = state.favouriteSongList.indexOf(action?.payload)
            state.favouriteSongList.splice(index,1)

            return{...state,favouriteSongList: state.favouriteSongList}
        // case
        case FETCH_FAVOURITE_SONG:
            return state

        default:
            console.log('song reducer')
            return state
    }
}